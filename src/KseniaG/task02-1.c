#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int main()
{
	char password[256];
	int i, j, k, Lowercase = 0, Uppercase = 0, Number = 0, length;
	printf("Enter the length of your password \n");
	scanf("%d", &length);
	if (length<3)
	{
		puts("Incorrect password!\n");
		return 1;
	}
	puts("Your passwords \n");
	srand(time(NULL));
	for (i = 0; i < 40; i++)
	{
		while ((Uppercase == 0) || (Lowercase == 0) || (Number == 0))
		{
			Uppercase == 0; Lowercase == 0; Number == 0;
			for (j = 0; j < length; j++)
			{
				k = rand() % 3;
				if (k == 0)
				{
					password[j] = rand() % ('9' - '0' + 1) + '0';
					Number = 1;
				}
				if (k == 1)
				{
					password[j] = rand() % ('z' - 'a' + 1) + 'a';
					Lowercase = 1;

				}
				if (k == 2)
				{
					password[j] = rand() % ('Z' - 'A' + 1) + 'A';
					Uppercase = 1;
				}
			}
		}
		for (j = 0; j < length; j++)
			 printf("%c", password[j]);
		if (i % 2 == 0)
			putchar('\t');
		else
			putchar('\n');
		Lowercase = 0; Uppercase = 0; Number = 0;
	}
	return 0;
}
